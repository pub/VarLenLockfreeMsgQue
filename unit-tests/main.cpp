#define CATCH_CONFIG_RUNNER
#define CATCH_CONFIG_ENABLE_BENCHMARKING

#include "catch.hpp"


int main( int argc, char *argv[] )
{
    int iRet = 0;
    {
        Catch::Session session;

        iRet = session.applyCommandLine( argc, argv );

        if ( iRet == 0 )
        {
            session.configData().shouldDebugBreak = true;
            session.configData().showDurations = Catch::ShowDurations::Always;

            const auto start = std::chrono::steady_clock::now();
            iRet = session.run();
            const auto stop = std::chrono::steady_clock::now();
            auto duration = stop - start;
            std::printf( "\n===============================================================================\n"
                         "Tests took %.3f sec(s)\n"
                         "===============================================================================\n\n",
                         duration.count() / 1000000000.0 );
        }
    }
    return iRet;
}
