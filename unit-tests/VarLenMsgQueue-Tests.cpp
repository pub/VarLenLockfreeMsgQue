#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include "catch.hpp"
#include <iostream>
#include <thread>

#include <VarLenMsgQueue.h>

static inline int set_thread_affinity( int tid, int iCPU )
{
    cpu_set_t mask;
    CPU_ZERO( &mask );
    CPU_SET( iCPU, &mask );
    return sched_setaffinity( tid, sizeof( mask ), &mask );
}

TEST_CASE( "BenchTest-CompareAndSwap" )
{
    return;
    struct DWord
    {
        size_t lo, hi;
    };

    const size_t NRepeat = 100;

    static std::array<std::atomic<size_t>, NRepeat> atomicInts;
    static std::array<std::atomic<DWord>, NRepeat> atomicDWords;

    BENCHMARK( "CAS-int64" )
    {
        for ( size_t i = 0; i < NRepeat; ++i )
        {
            auto x = i;
            atomicInts[i].compare_exchange_weak( x, i + 1 );
        }
    };
    BENCHMARK( "CAS-DWord" )
    {
        // std::atomic<size_t> x{ 0 };
        // x.compare_exchange_weak( 0, 1 );
        for ( size_t i = 0; i < NRepeat; ++i )
        {
            DWord x{ i, i };
            atomicDWords[i].compare_exchange_weak( x, DWord{ i, i + 1 } );
        }
    };
}

TEST_CASE( "VarLenMsgQueue.Basic" )
{
    using VLMRMQ = VarLenMultiRecieverQueue<>;
    constexpr size_t bufsize = 1024 * 10;
    constexpr size_t NumHeaderEntries = 32;

    constexpr size_t REMOTE_MSG_SIZE = VLMRMQ::PAYLOAD_ALIGNMENT; // 64

    // (bufsize - 640 - 64 * NumHeaderEntries)/64
    constexpr size_t NumPayloadEntries =
            ( bufsize - VLMRMQ::RINGBUFFER_HEADER_SIZE - NumHeaderEntries * VLMRMQ::HEADER_ENTRY_SIZE ) / VLMRMQ::PAYLOAD_ALIGNMENT;

    std::unique_ptr<char> pBuf( (char *)aligned_alloc( VLMRMQ::PAYLOAD_ALIGNMENT, bufsize ) );
    VLMRMQ vlmq;

    vlmq.initializeRingBuffer(
            pBuf.get(), bufsize, typename VLMRMQ::RingBufferInitParams{ .numMsgEntries = NumHeaderEntries, .numReceivers = 1 }, std::cerr );

    REQUIRE( vlmq.size() == 0 );
    REQUIRE( vlmq.capacity() == NumHeaderEntries );
    REQUIRE( vlmq.payload_size() == 0 );
    // as pBuf is aligned already, no align_up is needed.
    // 10240 - 640 - 32 * 64 = 7552
    REQUIRE( vlmq.payload_capacity() == ( bufsize - VLMRMQ::RINGBUFFER_HEADER_SIZE - NumHeaderEntries * VLMRMQ::HEADER_ENTRY_SIZE ) );

    //- send a message size <= INPLACE_PAYLOAD_SIZE (32 bytes)
    {
        VLMRMQ::AcquiredSend acquired;
        REQUIRE( VLMRMQ::AcquiredSend::OK == vlmq.acquireSend( acquired, VLMRMQ::INPLACE_PAYLOAD_SIZE ) );
        REQUIRE( acquired.getAcquiredPayloadSize() == VLMRMQ::INPLACE_PAYLOAD_SIZE ); // 32
        vlmq.commitSend( acquired );

        REQUIRE( vlmq.size() == 1 );
        REQUIRE( vlmq.payload_size() == 0 ); // inplace allocation.
    }

    //- send a message size = INPLACE_PAYLOAD_SIZE + 1(32 bytes)
    {
        VLMRMQ::AcquiredSend acquired;
        REQUIRE( VLMRMQ::AcquiredSend::OK == vlmq.acquireSend( acquired, VLMRMQ::INPLACE_PAYLOAD_SIZE + 1 ) );
        REQUIRE( acquired.getAcquiredPayloadSize() == REMOTE_MSG_SIZE ); // 64
        vlmq.commitSend( acquired );

        REQUIRE( vlmq.size() == 2 );
        REQUIRE( vlmq.payload_size() == acquired.getAcquiredPayloadSize() ); // remote allocation, 64
    }

    {
        VLMRMQ::AcquiredRecv acquired{ .receiverID = 0 };
        //- receive 1st inplace msg.

        REQUIRE( VLMRMQ::AcquiredRecv::OK == vlmq.acquireRecv( acquired ) );
        REQUIRE( acquired.pMsg->payloadLen == VLMRMQ::INPLACE_PAYLOAD_SIZE );
        vlmq.commitRecv( acquired );
        REQUIRE( vlmq.size() == 1 );
        REQUIRE( vlmq.payload_size() == REMOTE_MSG_SIZE ); // remote buffer still have 2nd message.

        //- receiv 2nd remote msg.
        REQUIRE( VLMRMQ::AcquiredRecv::OK == vlmq.acquireRecv( acquired ) );
        REQUIRE( acquired.pMsg->payloadLen == REMOTE_MSG_SIZE );
        vlmq.commitRecv( acquired );
        REQUIRE( vlmq.size() == 0 );
        REQUIRE( vlmq.payload_size() == 0 );
    }

    REQUIRE( vlmq.getSendPayloadPos() == REMOTE_MSG_SIZE );

    //- test header queue full
    // send messages until header queue is full. HeaderQueue is larger than payload queue.
    for ( size_t i = 0, N = vlmq.capacity() / REMOTE_MSG_SIZE; i < N - 1; ++i )
    {
        INFO( "i=" << i << ", N=" << N << ", nextSendPaylodPos=" << vlmq.getSendPayloadPos() );
        REQUIRE( vlmq.getSendPayloadPos() == REMOTE_MSG_SIZE * ( i + 1 ) );

        VLMRMQ::AcquiredSend acquired;
        size_t receiverID = 0;
        auto stat = vlmq.acquireSend( acquired, VLMRMQ::INPLACE_PAYLOAD_SIZE + 1 );
        if ( VLMRMQ::AcquiredSend::OK != stat )
        {
            REQUIRE( VLMRMQ::AcquiredSend::HEADER_QUEUE_FULL == stat );
            break;
        }
        REQUIRE( acquired.getAcquiredPayloadSize() == REMOTE_MSG_SIZE ); // 64
        vlmq.commitSend( acquired );
    }
    REQUIRE( vlmq.size() == vlmq.capacity() );
}

TEST_CASE( "VarLenMsgQueue.PayloadFull" )
{
    using VLMRMQ = VarLenMultiRecieverQueue<>;
    constexpr size_t bufsize = 1024 * 10;
    constexpr size_t NumHeaderEntries = 128;
    constexpr size_t NumReceivers = 2;

    // (bufsize - 640 - 64 * NumHeaderEntries)/64 = 22
    constexpr size_t NumPayloadEntries =
            ( bufsize - VLMRMQ::RINGBUFFER_HEADER_SIZE - NumHeaderEntries * VLMRMQ::HEADER_ENTRY_SIZE ) / VLMRMQ::PAYLOAD_ALIGNMENT;

    constexpr size_t REMOTE_MSG_SIZE = VLMRMQ::PAYLOAD_ALIGNMENT; // 64

    std::unique_ptr<char> pBuf( (char *)aligned_alloc( VLMRMQ::PAYLOAD_ALIGNMENT, bufsize ) );
    VLMRMQ vlmq;

    vlmq.initializeRingBuffer( pBuf.get(),
                               bufsize,
                               typename VLMRMQ::RingBufferInitParams{ .numMsgEntries = NumHeaderEntries, .numReceivers = NumReceivers },
                               std::cerr );

    REQUIRE( vlmq.size() == 0 );
    REQUIRE( vlmq.capacity() == NumHeaderEntries );
    REQUIRE( vlmq.payload_size() == 0 );
    // as pBuf is aligned already, no align_up is needed.
    REQUIRE( vlmq.payload_capacity() == ( bufsize - VLMRMQ::RINGBUFFER_HEADER_SIZE - NumHeaderEntries * VLMRMQ::HEADER_ENTRY_SIZE ) );

    // send until payload full
    for ( size_t i = 0; i < NumPayloadEntries; ++i )
    {
        INFO( "i=" << i << ", N=" << NumPayloadEntries << ", nextSendPaylodPos=" << vlmq.getSendPayloadPos() );
        REQUIRE( vlmq.getSendPayloadPos() == REMOTE_MSG_SIZE * i );

        VLMRMQ::AcquiredSend acquired;
        auto stat = vlmq.acquireSend( acquired, REMOTE_MSG_SIZE );
        if ( VLMRMQ::AcquiredSend::OK != stat )
        {
            REQUIRE( VLMRMQ::AcquiredSend::PAYLOAD_QUEUE_FULL == stat );
            break;
        }
        REQUIRE( acquired.getAcquiredPayloadSize() == REMOTE_MSG_SIZE ); // 64
        vlmq.commitSend( acquired );
    }
    REQUIRE( vlmq.size() == NumPayloadEntries );
    REQUIRE( vlmq.payload_size() == vlmq.payload_capacity() );

    //- consume the first msg.
    for ( uint16_t i = 0; i < NumReceivers; ++i )
    {
        VLMRMQ::AcquiredRecv acquired{ .receiverID = i };
        //- receive 1st inplace msg.

        REQUIRE( VLMRMQ::AcquiredRecv::OK == vlmq.acquireRecv( acquired ) );
        REQUIRE( acquired.pMsg->payloadLen == REMOTE_MSG_SIZE );
        REQUIRE( acquired.pMsg->seqNum == 1 );
        bool msgDestroyed = vlmq.commitRecv( acquired );
        if ( i + 1 == NumReceivers )
        {
            REQUIRE( msgDestroyed );
        }
    }

    //- continue to send a msg.
    {
        VLMRMQ::AcquiredSend acquired;
        REQUIRE( VLMRMQ::AcquiredSend::OK == vlmq.acquireSend( acquired, REMOTE_MSG_SIZE ) );
        REQUIRE( acquired.pMsg->seqNum == NumPayloadEntries + 1 );
        REQUIRE( acquired.getAcquiredPayloadSize() == REMOTE_MSG_SIZE ); // 64
        vlmq.commitSend( acquired );
    }
}

TEST_CASE( "VarLenMsgQueue.Perf-SendRecv" )
{
    using VLMRMQ = VarLenMultiRecieverQueue<>; // VarLenMultiRecieverQueue<>;

    constexpr size_t NSenders = 4;
    constexpr size_t NMsgsPerSender = 10000;
    constexpr size_t NMsgSlots = VLMRMQ::next_power2( NSenders * NMsgsPerSender );

    constexpr size_t msgPaylodSize = 16; //  VLMRMQ::PAYLOAD_ALIGNMENT ; // 64 > 32 bytes inplace payload.

    constexpr size_t bufsize = VLMRMQ::RINGBUFFER_HEADER_SIZE + ( VLMRMQ::HEADER_ENTRY_SIZE + msgPaylodSize ) * NMsgSlots;
    constexpr size_t NumHeaderEntries = NMsgSlots;
    constexpr size_t NumReceivers = 1;

    // (bufsize - 640 - 64 * NumHeaderEntries)/64
    constexpr size_t NumPayloadEntries =
            ( bufsize - VLMRMQ::RINGBUFFER_HEADER_SIZE - NumHeaderEntries * VLMRMQ::HEADER_ENTRY_SIZE ) / VLMRMQ::PAYLOAD_ALIGNMENT;


    std::vector<char> msgSrc( msgPaylodSize );

    std::unique_ptr<char> pBuf( (char *)aligned_alloc( VLMRMQ::PAYLOAD_ALIGNMENT, bufsize ) );
    VLMRMQ vlmq;

    vlmq.initializeRingBuffer( pBuf.get(),
                               bufsize,
                               typename VLMRMQ::RingBufferInitParams{ .numMsgEntries = NumHeaderEntries, .numReceivers = NumReceivers },
                               std::cerr );

    // vlmq.setClockFunc( &vlmq.nullTimeNanos ); // use null time to get highest performance.

    size_t NumMsgs = NMsgsPerSender * NSenders;
    std::atomic<int> senderStartCount{ 0 }, senderEndCount{ 0 };
    std::thread senderThrd[NSenders];


    for ( auto i = 0; i < NSenders; ++i )
    {
        senderThrd[i] = std::thread( [&, iSender = i] { // - perf test send
            VLMRMQ::AcquiredSend acquired;
            if ( senderStartCount.fetch_add( 1 ) + 1 != NSenders )
            {
                while ( senderStartCount.load() != NSenders )
                    continue;
            }
            // auto startSendTs = std::chrono::steady_clock::now();

            for ( size_t i = 0; i < NMsgsPerSender; )
            {
                if ( VLMRMQ::AcquiredSend::OK == vlmq.acquireSend( acquired, msgPaylodSize ) )
                {
                    memcpy( acquired.pPayload, msgSrc.data(), msgPaylodSize );
                    vlmq.commitSend( acquired );
                    ++i;
                }
            }
            senderEndCount.fetch_add( 1 );
            // auto duration = ( std::chrono::steady_clock::now() - startSendTs ).count();
            // std::cout << iSender << "-th sender    completed sending " << NMsgsPerSender << " messages, duration: " << duration
            //           << " ns, nanos/send: " << duration / NMsgsPerSender << std::endl;
        } );
    }

    while ( senderStartCount.load() != NSenders )
        continue;
    auto startSendTs = std::chrono::steady_clock::now();
    while ( senderEndCount.load() != NSenders )
        continue;
    auto sendduration = ( std::chrono::steady_clock::now() - startSendTs ).count();

    for ( auto i = 0; i < NSenders; ++i )
        senderThrd[i].join();

    std::cout << NSenders << " senders      have sent " << NumMsgs << " messages, duration: " << sendduration
              << " ns, nanos/send: " << sendduration / NumMsgs << std::endl;


    { // - perf test recev
        VLMRMQ::AcquiredRecv acquired{ .receiverID = 0 };
        auto startSendTs = std::chrono::steady_clock::now();

        for ( size_t i = 0; i < NumMsgs; ++i )
        {
            auto stat = vlmq.acquireRecv( acquired );
            assert( stat == VLMRMQ::AcquiredRecv::OK );
            vlmq.commitRecv( acquired );
        }
        auto duration = ( std::chrono::steady_clock::now() - startSendTs ).count();
        std::cout << "    Recv " << NumMsgs << " messages, duration: " << duration << " ns, nanos/recv: " << duration / NumMsgs << std::endl;
        REQUIRE( vlmq.getRecvHeaderEntryIndex( acquired.receiverID ) == NumMsgs );
    };
}

TEST_CASE( "VarLenMsgQueue.Perf-ConcurrentSendRecv" )
{
    using VLMRMQ = VarLenMultiRecieverQueue<>;
    constexpr size_t bufsize = 1024 * 1024 * 8;
    constexpr size_t NumHeaderEntries = 1024 * 32;
    constexpr size_t NumReceivers = 1;

    // (bufsize - 640 - 64 * NumHeaderEntries)/64 = 98284
    constexpr size_t NumPayloadEntries =
            ( bufsize - VLMRMQ::RINGBUFFER_HEADER_SIZE - NumHeaderEntries * VLMRMQ::HEADER_ENTRY_SIZE ) / VLMRMQ::PAYLOAD_ALIGNMENT;

    constexpr size_t REMOTE_MSG_SIZE = VLMRMQ::PAYLOAD_ALIGNMENT; // 64
    size_t msgPaylodSize = REMOTE_MSG_SIZE;

    std::vector<char> msgSrc( msgPaylodSize );

    std::unique_ptr<char> pBuf( (char *)aligned_alloc( VLMRMQ::PAYLOAD_ALIGNMENT, bufsize ) );
    VLMRMQ vlmq;

    vlmq.initializeRingBuffer( pBuf.get(),
                               bufsize,
                               typename VLMRMQ::RingBufferInitParams{ .numMsgEntries = NumHeaderEntries, .numReceivers = NumReceivers },
                               std::cerr );

    constexpr bool bHasTimestamp = true;

    constexpr size_t NMsgs = 10000; // number of messages to send

    constexpr int sendCPU = -1; // -1 to disable affinity
    constexpr int recvCPU = -1; // -1 to disable affinity
    constexpr size_t sendIntervalUs = 10; // 0 to disable.

    std::atomic<size_t> readyCount{ 0 };

    std::vector<int64_t> dur( NMsgs ); // send-to-recv duration.

    if ( bHasTimestamp )
        vlmq.setClockFunc( &vlmq.nowSystemTimeNanos );

    std::thread sendProc( [&] { // - perf test send
        if ( sendCPU != -1 )
        {
            set_thread_affinity( 0, sendCPU );
        }
        VLMRMQ::AcquiredSend acquired;
        if ( readyCount.fetch_add( 1 ) < 1 )
        {
            while ( readyCount.load() != 2 )
                continue;
        }

        auto startSendTs = std::chrono::steady_clock::now();

        for ( size_t i = 0; i < NMsgs; ++i )
        {
            auto stat = vlmq.acquireSend( acquired, msgPaylodSize );
            assert( stat == VLMRMQ::AcquiredSend::OK );
            memcpy( acquired.pPayload, msgSrc.data(), msgPaylodSize );
            vlmq.commitSend( acquired );
            if constexpr ( sendIntervalUs )
            {
                std::this_thread::sleep_for( std::chrono::microseconds( sendIntervalUs ) );
            }
        }
        auto duration = ( std::chrono::steady_clock::now() - startSendTs ).count();
        if ( !sendIntervalUs )
            std::cout << "    Send " << NMsgs << " messages, duration: " << duration << " ns, nanos/send: " << duration / NMsgs << std::endl;
        REQUIRE( vlmq.getSendSeqNum() == NMsgs + 1 );
    } );
    std::thread recvProc( [&] { // - perf test recev
        if ( recvCPU != -1 )
        {
            set_thread_affinity( 0, recvCPU );
        }
        VLMRMQ::AcquiredRecv acquired{ .receiverID = 0 };

        if ( readyCount.fetch_add( 1 ) < 1 )
        {
            while ( readyCount.load() != 2 )
                continue;
        }
        auto startSendTs = std::chrono::steady_clock::now();

        for ( size_t i = 0; i < NMsgs; )
        {
            auto stat = vlmq.acquireRecv( acquired );
            if ( stat == VLMRMQ::AcquiredRecv::OK )
            {
                if constexpr ( bHasTimestamp )
                    dur[i] = vlmq.nowNanos() - acquired.pMsg->timestamp;
                vlmq.commitRecv( acquired );
                ++i;
            }
        }
        auto duration = ( std::chrono::steady_clock::now() - startSendTs ).count();
        if ( !sendIntervalUs )
            std::cout << "    Recv " << NMsgs << " messages, duration: " << duration << " ns, nanos/recv: " << duration / NMsgs << std::endl;
        REQUIRE( vlmq.getRecvHeaderEntryIndex( acquired.receiverID ) == NMsgs );

        if constexpr ( bHasTimestamp )
        {
            std::sort( dur.begin(), dur.end() );
            std::cout << "    Send-to-Recv   NMsgs: " << NMsgs << "  MinTime: " << dur[0] << ",  50%: " << dur[NMsgs / 2]
                      << ",  90% : " << dur[NMsgs * 0.9] << ",  Max: " << dur.back() << std::endl;
        }
    } );

    sendProc.join();
    recvProc.join();
}

/*
    Send 10000 messages, duration: 429540 ns, nanos/send: 42
    Recv 10000 messages, duration: 361688 ns, nanos/recv: 36
0.002 s: VarLenMsgQueue.Perf-SendRecv
    Send-to-Recv   NMsgs: 10000  MinTime: 57,  50%: 93,  90% : 137,  Max: 2611639
0.637 s: VarLenMsgQueue.Perf-ConcurrentSendRecv

*/
