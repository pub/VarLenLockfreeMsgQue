#pragma once
#include <atomic>
#include <utility>
#include <vector>
#include <ostream>
#include <string.h>
#include <assert.h>

/// VarLenMsgQueue works as a lockfree queue that enables mutiple producers send messages of variable length. It works in 2 modes:
///   - MultiReceiver: Each receiver must receive a copy of message. The last receiver destroys the message slot.
///   - MultiConsumer: All receivers contend to consume exclusively messages.
/// \note Each message has a sequence number will may overflow.
///
/// Internally there are 2 circular queues:
///   - Presence map that has fixed number of flags indicating if a message slot has value or not.
///   - Msg payload queue that stores messages payloads.
///
/// Memory layout:
///   RingBufferHeader      |    Fixed numbef of HeaderEntries     |        var-length payload buffer
///

///========================================================================
///                       Multi-Receiver
///========================================================================

template<size_t HeaderEntrySizeT = 64, size_t MaxMsgReceiversT = 8>
struct VarLenMultiRecieverQueue
{
    static constexpr size_t HEADER_ENTRY_SIZE = HeaderEntrySizeT; // inplace payload size = HEADER_ENTRY_SIZE - 32
    static constexpr size_t MAX_MSG_RECEIVERS = MaxMsgReceiversT;
    static constexpr size_t PAYLOAD_ALIGNMENT = 64;

    using this_type = VarLenMultiRecieverQueue;

    using HalfWordUInt = uint32_t; // 32bit on 64bit machine; 16bit on 32bit machine.
    using MsgType = uint16_t;

    using SeqNum = size_t;
    using Timestamp = int64_t;

    using PayloadLen = uint32_t;
    using PayloadPos = size_t;
    using HeaderEntryID = HalfWordUInt;

    using GetNowFuncT = Timestamp ( * )();

    static Timestamp nowSystemTimeNanos()
    {
        return std::chrono::system_clock::now().time_since_epoch().count();
    }
    static Timestamp nullTimeNanos()
    {
        return -1;
    }

    struct PayloadLapPos
    {
        using ValueType = size_t;
        ValueType value;

        static constexpr size_t LAP_BITS = 2;
        static constexpr size_t PAYLOAD_POSITION_BITS = 30;

        static constexpr size_t MAX_PAYLOAD_POSITION = ( size_t( 1 ) << PAYLOAD_POSITION_BITS ) - 1;
        static constexpr ValueType LAP_BIT_MASK = ~( ( ValueType( 1 ) << PAYLOAD_POSITION_BITS ) - 1 );
        static constexpr ValueType LAP_ONE = ValueType( 1 ) << PAYLOAD_POSITION_BITS;

        size_t getPayloadPos() const
        {
            return value & MAX_PAYLOAD_POSITION;
        }
    };

    struct MsgAddress // Double Word
    {
        SeqNum seqNum; // HeaderEntryID  = (seqNum - InitialSeqNum + 1 ) % NumOfHeaderEntries

        PayloadLapPos payloadLapPos;

        size_t getPayloadPos() const
        {
            return payloadLapPos.getPayloadPos();
        }

        size_t getSeqNum() const
        {
            return seqNum;
        }

        bool differentLap( PayloadLapPos aLapAndPayloadPosition ) const
        {
            return ( payloadLapPos.value ^ aLapAndPayloadPosition.value ) >> PayloadLapPos::PAYLOAD_POSITION_BITS;
        }

        MsgAddress nextAddressKeepPayloadLap( typename PayloadLapPos::ValueType payloadPos ) const
        {
            assert( payloadPos.value <= PayloadLapPos::MAX_PAYLOAD_POSITION );
            return MsgAddress{ seqNum + 1, ( payloadLapPos.value & PayloadLapPos::LAP_BIT_MASK ) | payloadPos };
        }

        MsgAddress nextAddressIncPayloadLap( typename PayloadLapPos::ValueType payloadPos ) const
        {
            assert( payloadPos <= PayloadLapPos::MAX_PAYLOAD_POSITION );
            return MsgAddress{ seqNum + 1, ( ( payloadLapPos.value & PayloadLapPos::LAP_BIT_MASK ) + PayloadLapPos::LAP_ONE ) | payloadPos };
        }
    };
    // static_assert( std::atomic<MsgAddress>::is_always_lock_free, "MsgAddress must be lockfree" );

    struct MsgHeader : public MsgAddress
    {
        Timestamp timestamp; // 8 bytes. timestemp when sender completes sending.
        std::atomic<uint16_t> hasValue; // 2 bytes. copies of value to consume/receive.
        MsgType msgType; // 2 bytes. default to 0.
        PayloadLen payloadLen; // uint32_t. 4 bytes.

        //-- HEADER_ENTRY_SIZE - bytes above
        static constexpr size_t INPLACE_PAYLOAD_SIZE = HEADER_ENTRY_SIZE - 32; // 24 for 32 bits SeqNum; 32 for 64 bits SeqNum

        char inplaceStorage[INPLACE_PAYLOAD_SIZE]; // used only when payloadLen <= 32.

        bool usingInplaceStorage() const
        {
            return payloadLen == INPLACE_PAYLOAD_SIZE;
        }
    };
    static constexpr size_t INPLACE_PAYLOAD_SIZE = MsgHeader::INPLACE_PAYLOAD_SIZE; // 32 bytes

    static constexpr size_t MSG_HEADER_SIZE = sizeof( MsgHeader ); // 64 bytes
    static_assert( MSG_HEADER_SIZE == HEADER_ENTRY_SIZE, "64 bytes" );

    struct RecvState
    {
        HeaderEntryID m_recvEntryID; // next position.
        char padding[64 - sizeof( HeaderEntryID )];
    };
    static_assert( sizeof( RecvState ) == 64, "mutiple of 64" );

    struct RingBufferHeader
    {
        std::atomic<MsgAddress> iNextEnqueAcquire; // next position.

        // MAYDO: each msgType has a fixed number of receivers.
        // Or each receiver has a fixed number of instested msgTypes when msgTypes are represented in bits.
        uint16_t nExpectedReceivers;
        char paddings1[6];
        RecvState iReceiverPositions[MAX_MSG_RECEIVERS];
        // TODO: false cache line sharing padding.

        std::atomic<HeaderEntryID> nextDequeEntryID; // only used to estimate the queue size.
        std::atomic<PayloadLapPos> nextDequePayloadLapPos; // iNextEnqueAcquire should NOT surpass it.


        size_t numHeaderEntries; // fixed number of header entries.  must be power of 2.
        size_t headerEntriesStartPos; // offset in bytes
        size_t payloadBufferStartPos; // offset in bytes
        size_t payloadBufferSize;

        SeqNum initialSeqNum = 1; //

        size_t maxMsgPayloadSize = 256; // ring buffer must be at least of 4 msg size.
        char bufferName[32]; // 56 for 32 bits SeqNum; 32 for 64 bits SeqNum
    };

    static constexpr size_t RINGBUFFER_HEADER_SIZE = sizeof( RingBufferHeader ); // 640 bytes when 8 max receivers
    static_assert( RINGBUFFER_HEADER_SIZE == 640, "640 bytes" );

protected:
    GetNowFuncT m_pNowTimestamp = &nullTimeNanos;

    char *m_pBuf = nullptr;
    size_t m_bufSize;

    RingBufferHeader *m_pBufHeader = nullptr;

    //--- infered fields --------------

    MsgHeader *m_headers = nullptr;
    size_t m_numHeaders; // must be power of 2
    size_t m_numHeadersMask; // m_numHeaders -1
    SeqNum m_initialSeqNum;

    char *m_pMsgPayload = nullptr;
    size_t m_msgPayloadSize;

public:
    struct RingBufferInitParams
    {
        size_t numMsgEntries; // number of messages per lap.
        uint16_t numReceivers; // >=1
        uint64_t initialSeqNum = 1; // >=0
        size_t maxMsgPayloadSize = 1024 * 4; // ring buffer must be at least of 4 msg size.
    };

    /// Init RingBuffer and iniital this object also.
    bool initializeRingBuffer( char *buf, size_t bufsize, const RingBufferInitParams &params, std::ostream &err )
    {
        assert( params.initialSeqNum >= 1 );
        assert( params.numReceivers >= 1 ); // TODO can be hold in 16bits int.
        assert( bufsize <= MsgAddress::MAX_PAYLOAD_POSITION );

        m_pBuf = buf;
        m_bufSize = bufsize;
        m_pBufHeader = reinterpret_cast<RingBufferHeader *>( buf ); // MAYDO: 64bytes alignment

        size_t bufHeaderSize = sizeof( RingBufferHeader );
        if ( m_bufSize < sizeof( RingBufferHeader ) + params.numMsgEntries * MSG_HEADER_SIZE )
        {
            err << "Buffer is too small for " << params.numMsgEntries << " msg entries.";
            return false;
        }
        if ( params.numReceivers > MAX_MSG_RECEIVERS )
        {
            err << "Exceeded max number of receivers: " << MAX_MSG_RECEIVERS;
            return false;
        }
        if ( ( ( params.numMsgEntries - 1 ) & params.numMsgEntries ) != 0 || params.numMsgEntries == 0 )
        {
            err << "numMsgEntries must be power of 2.";
            return false;
        }

        //- init buffer header
        m_pBufHeader->iNextEnqueAcquire = MsgAddress{ params.initialSeqNum, 0 };
        // assert( m_pBufHeader->iNextEnqueAcquire.is_lock_free() && "must be aligned!" );
        m_pBufHeader->nextDequeEntryID = 0;
        m_pBufHeader->nextDequePayloadLapPos = PayloadLapPos{ 0 };
        m_pBufHeader->nExpectedReceivers = params.numReceivers;
        m_pBufHeader->initialSeqNum = params.initialSeqNum;
        m_pBufHeader->maxMsgPayloadSize = params.maxMsgPayloadSize;
        m_pBufHeader->numHeaderEntries = params.numMsgEntries;
        m_pBufHeader->headerEntriesStartPos = sizeof( RingBufferHeader ); // MAYDO: 64bytes alignment
        m_pBufHeader->payloadBufferStartPos =
                align_up( sizeof( RingBufferHeader ) + params.numMsgEntries * MSG_HEADER_SIZE, PAYLOAD_ALIGNMENT ); // MAYDO: 64bytes alignment
        m_pBufHeader->payloadBufferSize = m_bufSize - m_pBufHeader->payloadBufferStartPos;

        for ( size_t i = 0; i < params.numReceivers; ++i )
        {
            RecvState &state = m_pBufHeader->iReceiverPositions[i];
            state.m_recvEntryID = 0;
        }

        //- init header entries
        m_headers = reinterpret_cast<MsgHeader *>( m_pBuf + m_pBufHeader->headerEntriesStartPos );
        m_numHeaders = m_pBufHeader->numHeaderEntries;
        m_numHeadersMask = m_numHeaders - 1;
        m_initialSeqNum = m_pBufHeader->initialSeqNum;
        m_pMsgPayload = m_pBuf + m_pBufHeader->payloadBufferStartPos;
        m_msgPayloadSize = m_pBufHeader->payloadBufferSize;
        for ( size_t i = 0; i < m_numHeaders; ++i )
        {
            memset( &m_headers[i], 0, sizeof( MsgHeader ) );
        }

        return true;
    }

    /// set clock for senders. Receivers don't need to call this function.
    // by default, it's nowSystemTimeNanos. User can use nullTimeNanos save ~40ns.
    void setClockFunc( GetNowFuncT pFunc )
    {
        m_pNowTimestamp = pFunc;
    }

    /// Init this object from an already inited ring buffer.
    bool initFromRingBuffer( char *buf, size_t bufsize, std::ostream &err )
    {
        m_pBuf = buf, m_bufSize = bufsize;
        m_pBufHeader = reinterpret_cast<RingBufferHeader *>( buf ); // MAYDO: 64bytes alignment

        m_headers = reinterpret_cast<MsgHeader *>( m_pBuf + m_pBufHeader->headerEntriesStartPos );
        m_numHeaders = m_pBufHeader->numHeaderEntries;
        m_initialSeqNum = m_pBufHeader->initialSeqNum;
        m_pMsgPayload = m_pBuf + m_pBufHeader->payloadBufferStartPos;
        m_msgPayloadSize = m_pBufHeader->payloadBufferSize;

        return true;
    }


    //========================================================================
    //                  send
    //========================================================================

    struct AcquiredSend
    {
        enum Status
        {
            OK,
            RETRY_ON_CONTENTION,
            // system may stop when below errors.
            HEADER_QUEUE_FULL, // header entry queue is full
            PAYLOAD_QUEUE_FULL, // payload buffer is full
            PAYLOAD_EXCEEDS_MAX_SIZE,
        };

        MsgHeader *pMsg = nullptr; // acquired write buffer with msgLength field populated.
        char *pPayload = nullptr;

        /// \note that acquired payload size >= needed.
        size_t getAcquiredPayloadSize()
        {
            if ( !pMsg )
                return 0;
            return pMsg->payloadLen;
        }
    };

    /// \param msgPayloadSize: the min size of expected payload.
    /// if returns AcquiredSend::OK,  user must call commitSend(output)
    typename AcquiredSend::Status acquireSend( AcquiredSend &output, size_t msgPayloadSize )
    {
        assert( m_pBufHeader );

        if ( msgPayloadSize > m_pBufHeader->maxMsgPayloadSize )
            return AcquiredSend::PAYLOAD_EXCEEDS_MAX_SIZE;

        MsgAddress enqueAddr = m_pBufHeader->iNextEnqueAcquire.load();
        HeaderEntryID headerID = getHeaderEntryID( enqueAddr.getSeqNum() );
        MsgHeader &header = m_headers[headerID];

        if ( header.hasValue )
            return AcquiredSend::HEADER_QUEUE_FULL;
        if ( msgPayloadSize <= INPLACE_PAYLOAD_SIZE ) // use inplace payload.
        {
            if ( !m_pBufHeader->iNextEnqueAcquire.compare_exchange_weak( enqueAddr,
                                                                         enqueAddr.nextAddressKeepPayloadLap( enqueAddr.getPayloadPos() ) ) )
            {
                return AcquiredSend::RETRY_ON_CONTENTION;
            }
            acquireEnque( output, header, enqueAddr, INPLACE_PAYLOAD_SIZE, header.inplaceStorage );
            return AcquiredSend::OK;
        }

        //- now calculate next available payload.
        MsgAddress nextAddr;

        PayloadLapPos dequePayloadLapAndPos = m_pBufHeader->nextDequePayloadLapPos.load();
        size_t dequePayloadPos = dequePayloadLapAndPos.getPayloadPos();
        msgPayloadSize = align_up( msgPayloadSize, PAYLOAD_ALIGNMENT );

        assert( msgPayloadSize <= m_pBufHeader->maxMsgPayloadSize );

        size_t currEnquePayloadPos = enqueAddr.getPayloadPos();
        size_t nextPayloadPos = currEnquePayloadPos + msgPayloadSize; // next enqueue position

        if ( !enqueAddr.differentLap( dequePayloadLapAndPos ) ) // enque/deque on the same lap
        {
            assert( currEnquePayloadPos >= dequePayloadPos );
            if ( nextPayloadPos < m_msgPayloadSize ) // likely
                nextAddr = enqueAddr.nextAddressKeepPayloadLap( nextPayloadPos );
            else if ( nextPayloadPos == m_msgPayloadSize ) // unlikey: last payload is equal to requested size.
                nextAddr = enqueAddr.nextAddressIncPayloadLap( 0 ); // dequePayloadPos == nextPayloadPos
            else // unlikey: nextPayloadPos > m_msgPayloadSize , last slot is too small for current msg.
            {
                //- check if begining of next lap has enough room.
                if ( msgPayloadSize > dequePayloadPos ) // begining of next lap is still too small
                    return AcquiredSend::PAYLOAD_QUEUE_FULL;
                // else there's room at the begining of next lap.
                if ( !m_pBufHeader->iNextEnqueAcquire.compare_exchange_weak( enqueAddr, enqueAddr.nextAddressIncPayloadLap( msgPayloadSize ) ) )
                    return AcquiredSend::RETRY_ON_CONTENTION;
                // else acquired a msg slot.  skip the last memory.
                acquireEnque( output, header, enqueAddr, msgPayloadSize, m_pMsgPayload );
                return AcquiredSend::OK;
            }
        }
        else // deque cursor has one more lap
        {
            if ( nextPayloadPos > dequePayloadPos ) // unlikey: too small for msg.
            {
                return AcquiredSend::PAYLOAD_QUEUE_FULL;
            }
            // else it's large enough for the msg.
            nextAddr = enqueAddr.nextAddressKeepPayloadLap( nextPayloadPos );
        }

        if ( !m_pBufHeader->iNextEnqueAcquire.compare_exchange_weak( enqueAddr, nextAddr ) )
        {
            return AcquiredSend::RETRY_ON_CONTENTION;
        }
        else
        {
            acquireEnque( output, header, enqueAddr, msgPayloadSize, m_pMsgPayload + currEnquePayloadPos );
            return AcquiredSend::OK;
        }
    }

    void commitSend( AcquiredSend &acquired )
    {
        assert( m_pNowTimestamp );
        acquired.pMsg->timestamp = m_pNowTimestamp();
        acquired.pMsg->hasValue = m_pBufHeader->nExpectedReceivers + 1; // plus 1
    }

    //========================================================================
    //                  Receive
    //========================================================================

    struct AcquiredRecv
    {
        enum Status
        {
            OK,
            NO_MESSSAGE,
        };
        uint16_t receiverID = -1; // input.
        const MsgHeader *pMsg = nullptr; // output
        const char *pPayload = nullptr; // output
    };

    typename AcquiredRecv::Status acquireRecv( AcquiredRecv &output )
    {
        assert( output.receiverID < m_pBufHeader->nExpectedReceivers );
        RecvState &recvState = m_pBufHeader->iReceiverPositions[output.receiverID];
        MsgHeader &header = m_headers[recvState.m_recvEntryID];
        if ( !header.hasValue.load() )
            return AcquiredRecv::NO_MESSSAGE;
        output.pMsg = &header;
        output.pPayload = header.usingInplaceStorage() ? header.inplaceStorage : ( m_pMsgPayload + header.getPayloadPos() );
        return AcquiredRecv::OK;
    }

    /// \param onMsgDestroy call onMsgDestroy(char* payload, uint32_t len) when it's the last receiver and the messages will be destroyed.
    /// \return true if this is the last receive and the message is destroyed.
    template<class OnMsgDestroy = std::nullptr_t>
    bool commitRecv( AcquiredRecv &acquired, OnMsgDestroy &&onMsgDestroy = OnMsgDestroy{} )
    {
        assert( acquired.receiverID < m_pBufHeader->nExpectedReceivers );
        MsgHeader &header = *const_cast<MsgHeader *>( acquired.pMsg );
        assert( header.hasValue.load() > 1 );

        auto &recvEntryID = m_pBufHeader->iReceiverPositions[acquired.receiverID].m_recvEntryID;
        recvEntryID = ( recvEntryID + 1 ) & m_numHeadersMask;
        if ( header.hasValue.fetch_sub( 1 ) == 2 ) // it's the last copy.
        {
            if constexpr ( !std::is_same_v<OnMsgDestroy, std::nullptr_t> )
            {
                onMsgDestroy( const_cast<char *>( acquired.pPayload ), header.payloadLen );
            }
            // update nextDequePayloadLapPos for sender enqueue.
            MsgHeader &nextHeader = m_headers[recvEntryID];
            if ( nextHeader.hasValue )
                m_pBufHeader->nextDequePayloadLapPos = nextHeader.payloadLapPos;
            else // this is the only msg.
            { // nextDequePayloadLapPos may not point to the next availble slot.
                // When next entry is consumed, it may advance to a correct position.
                m_pBufHeader->nextDequePayloadLapPos =
                        header.usingInplaceStorage() ? header.payloadLapPos : PayloadLapPos{ header.payloadLapPos.value + header.payloadLen };
            }
            m_pBufHeader->nextDequeEntryID = recvEntryID; // advance deque cursor
            header.hasValue = 0; // release header entry
            return true;
        }
        return false;
    }

    //========================================================================
    //                  util functions
    //========================================================================


    int64_t nowNanos() const
    {
        return m_pNowTimestamp();
    }

    /// \return approx number of messages in queue.
    size_t size() const
    {
        size_t nextDequeID = m_pBufHeader->nextDequeEntryID;
        if ( m_headers[nextDequeID].hasValue )
        {
            size_t nextEnqueID = getHeaderEntryID( m_pBufHeader->iNextEnqueAcquire.load().seqNum );
            return nextEnqueID > nextDequeID ? ( nextEnqueID - nextDequeID ) : ( m_numHeaders - ( nextDequeID - nextEnqueID ) );
        }
        return 0;
    }

    /// \return capacity to store number of messages.
    size_t capacity() const
    {
        return m_numHeaders;
    }

    /// \return true if size() == 0
    bool empty() const
    {
        return size() == 0;
    }

    /// \return true if size() == capacity() or payload_size() == payload_capacity()
    bool full() const
    {
        return size() == capacity() || payload_size() == payload_capacity();
    }

    /// \return approx payload in use.
    size_t payload_size() const
    {
        PayloadLapPos nextDequeLapPos = m_pBufHeader->nextDequePayloadLapPos;
        MsgAddress nextEnqueAddr = m_pBufHeader->iNextEnqueAcquire.load();
        size_t nextDequePayloadPos = nextDequeLapPos.getPayloadPos();
        size_t nextEnquePayloadPos = nextEnqueAddr.getPayloadPos();

        if ( nextEnqueAddr.differentLap( nextDequeLapPos ) )
        {
            assert( nextEnquePayloadPos <= nextDequePayloadPos );
            return m_msgPayloadSize - ( nextDequePayloadPos - nextEnquePayloadPos );
        }
        else
        {
            assert( nextEnquePayloadPos >= nextDequePayloadPos );
            return nextEnquePayloadPos - nextDequePayloadPos;
        }
    }

    size_t payload_capacity() const
    {
        return m_msgPayloadSize;
    }

    size_t getRecvPayloadPos() const
    {
        return m_pBufHeader->nextDequePayloadLapPos & ( ~MsgAddress::LAP_BIT_MASK );
    }

    size_t getSendPayloadPos() const
    {
        MsgAddress nextEnqueAddr = m_pBufHeader->iNextEnqueAcquire.load();
        return nextEnqueAddr.getPayloadPos();
    }

    size_t getSendPayloadLap() const
    {
        MsgAddress nextEnqueAddr = m_pBufHeader->iNextEnqueAcquire.load();
        return nextEnqueAddr.payloadLapPos >> MsgAddress::LAP_BITS;
    }

    size_t getRecvPayloadLap() const
    {
        return m_pBufHeader->nextDequePayloadLapPos >> MsgAddress::LAP_BITS;
    }

    SeqNum getSendSeqNum() const
    {
        return m_pBufHeader->iNextEnqueAcquire.load().getSeqNum();
    }

    size_t getSendHeaderEntryIndex() const
    {
        return getHeaderEntryID( getSendSeqNum() );
    }

    size_t getRecvHeaderEntryIndex( size_t recvID ) const
    {
        assert( recvID < m_pBufHeader->nExpectedReceivers );
        RecvState &recvState = m_pBufHeader->iReceiverPositions[recvID];
        return recvState.m_recvEntryID;
    }

    // Result is >= n
    template<class Int>
    static inline constexpr Int align_up( Int n, Int uiAlignment )
    {
        assert( n > 0 );
        //        assert( is_pow2( uiAlignment ) );
        return ( n + ( uiAlignment - 1 ) ) & ~( uiAlignment - 1 );
    }

    static constexpr uint32_t next_power2( uint32_t v )
    {
        v--;
        v |= v >> 1;
        v |= v >> 2;
        v |= v >> 4;
        v |= v >> 8;
        v |= v >> 16;
        v++;
        return v;
    }

protected:
    inline static void acquireEnque( AcquiredSend &output, MsgHeader &header, const MsgAddress &enqueAddr, size_t msgPayloadSize, char *pPayload )
    {
        header.seqNum = enqueAddr.seqNum;
        header.payloadLapPos = enqueAddr.payloadLapPos;
        header.payloadLen = msgPayloadSize;
        header.msgType = 0;

        output.pMsg = &header;
        output.pPayload = pPayload;
    }

    inline HeaderEntryID getHeaderEntryID( SeqNum seqNum ) const
    {
        return ( seqNum - m_initialSeqNum ) & m_numHeadersMask;
    }
};

///========================================================================
///                       Multi-Consumer
///========================================================================

struct VarLenMultiConsumerQueue
{
};